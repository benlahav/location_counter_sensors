"""This module is responsible for fetching api requests for GPIO pin state"""
import re
import socket
from typing import Dict, Optional, Tuple

from consts import API_ENDPOINT, SOCKET_PORT
from io_handlers import updated_state_pins, get_new_state


def msg_decoder(msg: bytes) -> Optional[Tuple[str, str]]:
    """
    Function that gets bytes message from server and manipulate with regex
    to unzip the pin id and the state

    :param msg: bytes massage
    :return: pin id anf state
    """
    pattern = re.compile(r"^(\d+)\s*:\s*(\d+)$")
    msg = str(msg, "utf-8")
    try:
        info = re.search(pattern, msg).groups()
        pin_id, state = info[0], info[1]
    except:
        pin_id, state = None, None

    return pin_id, state


def get_io_state_from_api(pins: Dict) -> None:
    """
    This function listens for message and returns
    data that got from the server when sent.
    PUSH notification from socket server.
    """
    with socket.socket() as c:
        c.connect((API_ENDPOINT, SOCKET_PORT))
        while True:
            try:
                # size of buffer is 8
                msg = c.recv(8)
                pin, state = msg_decoder(msg)

                if pin is not None and pin in pins.keys():
                    pins[pin] = get_new_state(pins[pin], int(state))
                    c.send(b'From client: got the message and changed state')
            except socket.error:
                c.send(b'From client: did not got the message')


def set_io_state_to_api(pins: Dict) -> None:
    """
    Function that sends via socket connection
    to server new pin state to save in DB.

    :param pins: the pins we want to update their state in server
    """
    try:
        with socket.socket() as c:
            c.connect((API_ENDPOINT, SOCKET_PORT))
            while True:
                to_change = updated_state_pins(pins)
                # todo: maybe we dont need this if we connect LED to Pi directly
                # get_new_state(to_change)
                for key, value in to_change.items():
                    msg = bytes(f"{key}: {value}")
                    c.send(msg)
    except Exception:
        set_io_state_to_api(pins)
