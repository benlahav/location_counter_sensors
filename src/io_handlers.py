"""This module helps connecting to raspberry pi and update GPIO state"""

from typing import List, Dict

from gpiozero import LED


def updated_state_pins(prev_state: Dict) -> Dict:
    """
    This function gets previous device pin state and check
    which pins changed state and return them.

    :param prev_state: set of pins state
    :return: set of updated pins, empty list if nothing changed
    """
    curr_state = {f"{pin}": led for pin, led in prev_state.items()}
    diff = dict()
    for prev_key, prev_value, curr_key, curr_value in zip(prev_state.items(), curr_state.items()):
        if curr_key == prev_key and curr_value != prev_value:
            diff[curr_value] = curr_state
    return diff


def get_new_state(prev_pin: LED, new_state: int) -> int:
    """
    Function that turns on or off a devices voltage by a given device pin in
    the raspberry pi and a given state.

    :param new_state: the new state we need to update to
    :param prev_pin: the LED object that is tied to th pin number
    :return: the new state 1 or 0
    """
    try:
        prev_state = prev_pin.value
        if prev_state != new_state:
            return new_state
        else:
            return prev_state
    except:
        pass


def init_gpio_pins(device_pins: List) -> Dict:
    """
    Configure pin as an output (no inputs in this project for now).
    Setup each one as output with LOW initial state.

    :param device_pins: List of the pins to configure
    :return set of dictionaries with pin id and his state
    """
    return {f"{device_pin}": LED(device_pin) for device_pin in device_pins}
