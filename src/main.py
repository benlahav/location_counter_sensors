"""This is Main Module"""
from concurrent.futures import ThreadPoolExecutor

import RPi.GPIO as GPIO

from io_handlers import init_gpio_pins
from api_handlers import get_io_state_from_api, set_io_state_to_api
from consts import DEVICE_PINS


def run_app() -> None:
    """Runner function of the app, create the threads to run"""
    # The mode of the GPIO that we work with (BCM = Broadcom SOC Channel)

    # GPIO.setmode(GPIO.BCM)

    pins = init_gpio_pins(DEVICE_PINS)

    try:
        with ThreadPoolExecutor(max_workers=2) as executor:
            # listener for changes in server
            executor.submit(get_io_state_from_api, pins)
            # sender of the new states to server
            executor.submit(set_io_state_to_api, pins)
    except Exception as error:
        print(error)


if __name__ == '__main__':
    run_app()
