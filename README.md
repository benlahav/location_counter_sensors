## Project Summary
* This project is part of LocationCounter project in HIT college.
* This is an industrial project the college does with reviewer from the industry.
* The project allow UI users (UI service project in here: https://gitlab.com/bukai7ben/LocationCounter) to change classrooms's power switches state from app.
* This part in the project is this app that should be downloaded and run on raspberry pi devices that are connected via electric wires to relays and then to classroom power switches.

## Overview (our sensors service)
* This project should be downloaded to each raspberry pi.
* We run for each pin thats connected to the pi a thread.
* This Thread loops and listens for changed in classrooms power switches.
* If someone manually turn ON/OFF then the thread of this pin number changes state and let the server know about it with a POST method.
* We also run a different thread for REST Push notification listening.
* For this solution we create a socket connection between the PI and the server.
* If someone changed state in the server (different team of the project - android or web app) then we know we should change the state in the classroom.
